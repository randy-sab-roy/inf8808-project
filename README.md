# Spotify Visualization

This repo contains the code for the [Spotify Visualization website](https://randy-sab-roy.gitlab.io/inf8808-project/) that was created as part of the INF8808 course of Polytechnique Montréal. The project was made with [React](https://reactjs.org/) and [D3](https://d3js.org/).

## Content

- `src/app.jsx`: The entry point of the React application.
- `src/data`: The data used in the visualizations in json.
- `src/presentation`: The welcome screen.
- `src/selection`: The country selection panel.
- `src/visualizations`: The visualization panel. Each visualization is a React component in the `graphs` sub-folder.

## Build in development mode

```
> npm install
> npm start
```

## Build in production mode

```
> npm run build
```
