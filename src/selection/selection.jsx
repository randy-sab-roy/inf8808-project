import "./selection.css";
import countries from "../data/countries";
import { ItemGroup } from "./itemGroup";

export function Selection(props) {
  const { selected, onSelect, onSelectNone, onSelectAll } = props;

  let groups = { NA: [], SA: [], EU: [], Asia: [], Oceania: [] };
  Object.entries(countries).forEach(c => {
    groups[c[1].Region] = groups[c[1].Region] == null ? [c] : [...groups[c[1].Region], c];
  });
  groups = Object.entries(groups).map(g => [g[0], g[1].sort((a, b) => (a[1].Name < b[1].Name ? -1 : 1))]);
  function createItemGroup(g) {
    return <ItemGroup code={g[0]} selected={selected} onSelect={onSelect} list={g[1]} />;
  }

  return (
    <div className="selection-container">
      <button className="util-button" onClick={() => onSelectAll(Object.keys(countries))}>
        Select All
      </button>
      <button className="util-button" onClick={onSelectNone}>
        Select None
      </button>
      <div className="button-col">
        {createItemGroup(groups[0])}
        {createItemGroup(groups[1])}
      </div>
      <div className="button-col">{createItemGroup(groups[2])}</div>
      <div className="button-col">
        {createItemGroup(groups[3])}
        {createItemGroup(groups[4])}
      </div>
    </div>
  );
}
