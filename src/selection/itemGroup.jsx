import React from "react";
import { COLOR_CONTINENTS } from "../constants";
import { Item } from "./item";
import "./item.css";

export function ItemGroup(props) {
  const { selected, list, onSelect, code } = props;

  return (
    <div>
      {list.map(c => (
        <Item
          key={c[0]}
          code={c[0]}
          checked={selected.includes(c[0])}
          onChange={() => onSelect(c[0])}
          name={c[1].Name}
          color={COLOR_CONTINENTS(code)}
        />
      ))}
    </div>
  );
}
