import React from "react";
import "./item.css";
import * as d3 from "d3";

export function Item(props) {
  const { code, checked, onChange, name, color } = props;

  const customColor = {
    "--buttonColor": d3.color(color).darker(4),
    "--buttonColorHover": d3.color(color).darker(2),
    "--buttonColorPressed": color,
    "--buttonColorPressedHover": d3.color(color).brighter(0.4),
  };

  return (
    <div style={customColor}>
      <input id={`toggle-${code}`} type="checkbox" className="toggleItem" checked={checked} onChange={onChange} />
      <label htmlFor={`toggle-${code}`}>{name}</label>
    </div>
  );
}
