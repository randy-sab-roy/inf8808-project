import { useEffect, useRef } from "react";
import * as d3 from "d3";
import top_artists from "../../data/top_of_the_week.json";
import countries from "../../data/countries.json";
import { COLOR_CONTINENTS } from "../../constants";
import { Title } from "../title";

const DESCRIPTION = `
  Works for 1 country selected.\n
  This visualization shows the most popular artists in this country.
  The value in Y represents how many times this artist had the most poopular song of the week.
`;

const MARGIN_LABELS = 10;
const MARGIN = 50;
const WIDTH = 800;
const HEIGHT = 300;

export function TopArtists(props) {
  const selection = props.selection;
  const svgRef = useRef();
  const tooltipRef = useRef();

  const show = selection.length === 1;

  useEffect(() => {
    if (!show) return;
    let g = d3.select(svgRef.current).select("g");

    if (g.node() == null) {
      g = d3.select(svgRef.current).attr("viewBox", `0 0 ${WIDTH} ${HEIGHT}`).append("g");
    }

    const tooltip = d3.select(tooltipRef.current);
    let data = top_artists[selection[0]].filter(a => a.Stream > 0).sort((b, a) => a.Stream - b.Stream);
    data = data.slice(0, Math.min(20, data.length));
    const maxTop = d3.max(data, d => d.Stream);

    // Scales
    const yScale = getScaleY(maxTop, HEIGHT, MARGIN, MARGIN_LABELS);
    const hScale = getScaleH(maxTop, HEIGHT, MARGIN, MARGIN_LABELS);
    const xScale = getScaleX(data, WIDTH, MARGIN, MARGIN_LABELS);

    // Add axis
    applyYAxis(g, yScale, MARGIN, MARGIN_LABELS);
    applyXAxis(g, xScale, HEIGHT, MARGIN, MARGIN_LABELS);

    // Add labels
    applyXAxisLabel(g, WIDTH, HEIGHT);
    applyYAxisLabel(g, HEIGHT, MARGIN_LABELS);

    g.selectAll(".barchart")
      .data(data)
      .join("g")
      .attr("class", "barchart")
      .append("rect")
      .attr("x", d => xScale(d.Artist))
      .attr("y", d => yScale(d.Stream))
      .attr("height", d => hScale(d.Stream))
      .attr("width", xScale.bandwidth())
      .attr("fill", COLOR_CONTINENTS(countries[selection[0]].Region))
      .on("mouseenter", (current, i, a) => {
        const others = g.selectAll(".barchart").filter(d => d !== current);
        others.select("rect").attr("opacity", 0.6);
        others
          .append("text")
          .attr("class", "difference")
          .attr("x", d => xScale(d.Artist) + xScale.bandwidth() - MARGIN_LABELS)
          .attr("y", d => yScale(d.Stream) + MARGIN_LABELS)
          .attr("fill", "white")
          .attr("text-anchor", "middle")
          .attr("font-size", 8)
          .text(e => {
            const difference = (e.Stream - current.Stream).toFixed(0);
            return (difference < 0 ? "" : "+") + difference;
          });

        // Add tooltip
        const barBox = a[i].getBoundingClientRect();
        tooltip
          .style("visibility", "visible")
          .style("left", `${barBox.left + barBox.width / 2}px`)
          .style("top", `${barBox.top}px`)
          .html(`<b>${current.Stream}</b>`);
      })
      .on("mouseleave", () => {
        tooltip.style("visibility", "hidden");
        g.selectAll(".barchart").select("rect").attr("opacity", 1);
        g.selectAll(".difference").remove();
      });
  }, [selection, show]);

  const title = `Most Popular Artists in ${countries[selection[0]].Name}`;

  const card = (
    <div className="card">
      <Title title={title} description={DESCRIPTION} />
      <div className="tooltip" ref={tooltipRef}></div>
      <svg ref={svgRef} className="graph"></svg>
    </div>
  );

  return show ? card : null;
}

//  Scales
function getScaleY(maxTop, HEIGHT, MARGIN, MARGIN_LABELS) {
  return d3.scaleLinear([0, maxTop], [HEIGHT - MARGIN - 3 * MARGIN_LABELS, MARGIN_LABELS]);
}

function getScaleH(maxTop, HEIGHT, MARGIN, MARGIN_LABELS) {
  return d3.scaleLinear([0, maxTop], [0, HEIGHT - MARGIN - 4 * MARGIN_LABELS]);
}

function getScaleX(data, WIDTH, MARGIN, MARGIN_LABELS) {
  let xScale = d3
    .scaleBand()
    .domain(data.map(d => d.Artist))
    .range([MARGIN + MARGIN_LABELS, WIDTH - MARGIN])
    .padding(0.5);

  return xScale;
}

// Axis
function applyXAxis(g, xScale, HEIGHT, MARGIN, MARGIN_LABELS) {
  let xAxis = g
    .append("g")
    .attr("transform", `translate(${0}, ${HEIGHT - MARGIN - 3 * MARGIN_LABELS})`)
    .call(d3.axisBottom(xScale))
    .selectAll("text")
    .style("text-anchor", "end")
    .attr("transform", "translate(-10,0)rotate(-45)");

  return xAxis;
}

function applyYAxis(g, yScale, MARGIN, MARGIN_LABELS) {
  let yAxis = g
    .append("g")
    .attr("transform", `translate(${MARGIN + MARGIN_LABELS}, ${0})`)
    .call(d3.axisLeft(yScale));

  return yAxis;
}

// Labels
function applyXAxisLabel(g, WIDTH, HEIGHT) {
  g.append("text")
    .attr("fill", "white")
    .attr("class", "axis-label")
    .attr("transform", `translate(${WIDTH / 2}, ${HEIGHT})`)
    .attr("text-anchor", "middle")
    .text("Artist");
}

function applyYAxisLabel(g, HEIGHT, MARGIN_LABELS) {
  g.append("text")
    .attr("fill", "white")
    .attr("class", "axis-label")
    .attr("text-anchor", "middle")
    .attr("transform", `translate(${MARGIN_LABELS}, ${HEIGHT / 2}) rotate(-90)`)
    .text("Number of Weeks at First Place");
}
