import { useEffect, useRef } from "react";
import * as d3 from "d3";
import countries from "../../data/countries.json";
import countryArtists from "../../data/country_artists.json";
import { Title } from "../title";
import { COLOR_CONTINENTS } from "../../constants";

const WIDTH = 800;
const HEIGHT = 250;
const RADIUS = 100;
const DARKER = 1.2;
const STROKE = 3;
const OFFSET = 2.2;
const DESCRIPTION = `
    Works for 2 countries selected.\n
    This visualization (Venn diagram) shows how many artists are shared between two countries (in the middle section) 
    and how many artists are not shared between the two countries (on either side). 
  `;

export function SharedArtists(props) {
  const selection = props.selection;
  const svgRef = useRef();
  const tooltipRef = useRef();

  const show = selection.length === 2;

  useEffect(() => {
    if (!show) return;

    let g = d3.select(svgRef.current).select("g");

    if (g.node() == null) {
      g = d3.select(svgRef.current).attr("viewBox", `0 0 ${WIDTH} ${HEIGHT}`).append("g");

      // Background
      g.append("circle")
        .attr("cx", WIDTH / 2 - RADIUS / 2)
        .attr("cy", HEIGHT / 2)
        .attr("r", RADIUS)
        .attr("fill", "black");
      g.append("circle")
        .attr("cx", WIDTH / 2 + RADIUS / 2)
        .attr("cy", HEIGHT / 2)
        .attr("r", RADIUS)
        .attr("fill", "black");

      // Borders
      g.append("circle")
        .attr("cx", WIDTH / 2 - RADIUS / 2)
        .attr("cy", HEIGHT / 2)
        .attr("r", RADIUS)
        .attr("fill", "none")
        .attr("stroke", "white")
        .attr("stroke-width", STROKE);
      g.append("circle")
        .attr("cx", WIDTH / 2 + RADIUS / 2)
        .attr("cy", HEIGHT / 2)
        .attr("r", RADIUS)
        .attr("fill", "none")
        .attr("stroke", "white")
        .attr("stroke-width", STROKE);

      // Arrows
      g.append("line")
        .attr("stroke", "white")
        .attr("stroke-linecap", "round")
        .attr("stroke-width", STROKE / 2)
        .attr("x1", WIDTH / 2 - OFFSET * RADIUS)
        .attr("x2", WIDTH / 2 - 1.5 * RADIUS)
        .attr("y1", HEIGHT / 3)
        .attr("y2", HEIGHT / 2);

      g.append("line")
        .attr("stroke", "white")
        .attr("stroke-linecap", "round")
        .attr("stroke-width", STROKE / 2)
        .attr("x1", WIDTH / 2 + 1.5 * RADIUS)
        .attr("x2", WIDTH / 2 + OFFSET * RADIUS)
        .attr("y1", HEIGHT / 2)
        .attr("y2", HEIGHT / 3);
    }

    const data = selection.map(s => countries[s]).slice(0, 2);
    g.selectAll(".venn")
      .data(data)
      .join("circle")
      .attr("class", "venn")
      .attr("cx", (_, i) => WIDTH / 2 + ((i === 0 ? -1 : 1) * RADIUS) / 2)
      .attr("cy", HEIGHT / 2)
      .attr("r", RADIUS)
      .style("mix-blend-mode", "screen")
      .attr("fill", d => d3.color(COLOR_CONTINENTS(d.Region)).darker(DARKER));

    g.selectAll(".name")
      .data(data)
      .join("text")
      .attr("class", "name")
      .attr("fill", "white")
      .attr("text-anchor", "middle")
      .attr("dy", "-0.4em")
      .attr("transform", (_, i) => `translate(${WIDTH / 2 + (i === 0 ? -1 : 1) * OFFSET * RADIUS}, ${HEIGHT / 3})`)
      .text(d => d.Name);

    const tooltip = d3.select(tooltipRef.current);

    const totalC1 = countryArtists[selection[0]];
    const totalC2 = countryArtists[selection[1]];
    const common = totalC1.filter(e => totalC2.includes(e));
    const intersection = common.length;
    const values = [totalC1.length - intersection, intersection, totalC2.length - intersection];

    g.selectAll(".venn")
      .on("mouseover", (d, i, a) => {
        const formatedText = common
          .slice(0, Math.min(10, common.length))
          .map(artist => `<li>${artist}</li>`)
          .join("");
        tooltip
          .style("visibility", "visible")
          .style("left", `${d3.event.pageX}px`)
          .style("top", `${d3.event.pageY + 5}px`)
          .html(`<b>Top common Artists</b></br><ol style="text-align: left;">${formatedText}</ol>`);
      })
      .on("mousemove", () => {
        tooltip.style("left", `${d3.event.pageX}px`).style("top", `${d3.event.pageY + 5}px`);
      })
      .on("mouseleave", (_, i, a) => {
        tooltip.style("visibility", "hidden");
      });

    g.selectAll(".value")
      .data(values)
      .join("text")
      .attr("font-size", "1.3em")
      .attr("dy", "0.2em")
      .attr("class", "value")
      .attr("fill", "white")
      .attr("text-anchor", "middle")
      .attr("transform", (_, i) => `translate(${WIDTH / 2 + (i - 1) * RADIUS}, ${HEIGHT / 2})`)
      .text(d => d);
  }, [selection, show]);

  const title = `Artists Shared Between 
  ${show ? countries[selection[0]].Name : ""} and 
  ${show ? countries[selection[1]].Name : ""}
  `;

  const card = (
    <div className="card">
      <Title title={title} description={DESCRIPTION} />
      <svg ref={svgRef} className="graph" style={{ margin: "20px 0" }}></svg>
      <div className="tooltip" ref={tooltipRef}></div>
    </div>
  );

  return show ? card : null;
}
