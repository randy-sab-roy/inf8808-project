import topSongs from "../../data/top_songs.json";
import { Title } from "../title";

const MAX_SONGS = 9;
const DESCRIPTION = 
`
Works for any number of countries selected, if there are songs to display.
These are the songs that were "Top of the Week" the most times in all selected countries. 
If a song never reached "Top of the Week" in any of the selected countries, it will not appear here.
`;

export function Playlist(props) {
  const selection = props.selection;
  const topSelected = selection.map(s => topSongs[s]);

  const counts = {};
  const scores = {};
  topSelected.forEach(c =>
    Object.entries(c).forEach(s => {
      counts[s[0]] = (counts[s[0]] || 0) + 1;
      scores[s[0]] = (scores[s[0]] || 0) + s[1];
    })
  );

  const commonSongs = Object.entries(counts)
    .filter(e => e[1] === selection.length)
    .map(e => [e[0], scores[e[0]]])
    .sort((a, b) => (a[1] > b[1] ? -1 : 1))
    .slice(0, MAX_SONGS)
    .map(e => e[0]);

  const card = (
    <div className="card">
      <Title title="Top Songs from the Selected Countries" description={DESCRIPTION} />
      <div className="embedded-container">
        {commonSongs.map(s => {
          return (
            <div key={s}>
              <iframe
                title={s}
                src={`https://open.spotify.com/embed/track/${s}`}
                width="300"
                height="80"
                frameBorder="0"
                allowtransparency="true"
                allow="encrypted-media"
              ></iframe>
            </div>
          );
        })}
      </div>
    </div>
  );

  return commonSongs.length > 0 ? card : null;
}
