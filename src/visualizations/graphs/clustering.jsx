import { useEffect, useRef } from "react";
import * as d3 from "d3";
import countries from "../../data/countries.json";
import { COLOR_CONTINENTS } from "../../constants";
import { Title } from "../title";

const DESCRIPTION = `
    Works for 3 countries or more selected. \n
    This visualization maps the countries onto a 2D chart using their similarity to place them (close is similar, far is different).
    To get a perfect similarity, two countries would need to have the exact same songs in their respective top 200, in the exact same order, for any given date.
    To get the minimum similarity possible, two countries would need to have exactly 0 songs in common in the top 200 for any given date.
    In between, an average similarity would require the two countries to have a sizeable propotion of songs in common, with somewhat similar rankings in the top 200 (for any given date). 
    This similarity index is calculated for each pair of countries and used in an algorithm (t-SNE) 
    that allows high-dimensional data to be placed in 2D on the chart.
  `;

const WIDTH = 800;
const HEIGHT = 400;
const RADIUS = 8;
const PADDING = 20;
const INNER_PADDING = 80;
const OPACITY = 0.6;
const TRANSITION = d3.transition().duration(200).ease(d3.easeLinear);

export function Clustering(props) {
  const selection = props.selection;
  const svgRef = useRef();
  const tooltipRef = useRef();

  const data = selection.map(s => ({ Country: s, ...countries[s] }));

  useEffect(() => {
    if (data.length < 1) return;

    let g = d3.select(svgRef.current).select("g");
    const tooltip = d3.select(tooltipRef.current);

    if (g.node() == null) {
      g = d3.select(svgRef.current).attr("viewBox", `0 0 ${WIDTH} ${HEIGHT}`).append("g");

      // Background
      g.append("rect")
        .attr("x", PADDING)
        .attr("y", PADDING)
        .attr("height", HEIGHT - 2 * PADDING)
        .attr("width", WIDTH - 2 * PADDING)
        .attr("fill", "#202020")
        .attr("stroke", "white")
        .attr("stroke-width", 1);
    }

    const xCoords = data.map(c => c.x);
    const yCoords = data.map(c => c.y);

    const xScale = d3.scaleLinear([d3.min(xCoords), d3.max(xCoords)], [INNER_PADDING, WIDTH - INNER_PADDING]);
    const yScale = d3.scaleLinear([d3.min(yCoords), d3.max(yCoords)], [HEIGHT - INNER_PADDING, INNER_PADDING]);

    const projectedData = data.map(d => ({ ...d, x: xScale(d.x), y: yScale(d.y) }));

    // Circles
    const circles = g.selectAll(".cluster-circle").data(projectedData, d => d.Country);
    g.selectAll("*").interrupt();
    circles
      .enter()
      .append("circle")
      .attr("cx", c => c.x)
      .attr("cy", c => c.y)
      .merge(circles)
      .attr("class", "cluster-circle")
      .attr("r", RADIUS)
      .attr("fill", c => COLOR_CONTINENTS(c.Region))
      .attr("fill-opacity", OPACITY)
      .attr("stroke", c => COLOR_CONTINENTS(c.Region))
      .attr("stroke-width", 0.7)
      .on("mouseover", (d, i, a) => {
        const pos = a[i].getBoundingClientRect();
        tooltip
          .style("visibility", "visible")
          .style("left", `${pos.left + pos.width / 2}px`)
          .style("top", `${pos.top}px`)
          .html(`<b>${d.Name}</b>`);
        d3.select(a[i]).attr("fill-opacity", 1).raise();
      })
      .on("mouseleave", (_, i, a) => {
        tooltip.style("visibility", "hidden");
        d3.select(a[i]).attr("fill-opacity", OPACITY);
      })
      .transition(TRANSITION)
      .attr("cx", c => c.x)
      .attr("cy", c => c.y);
    circles.exit().remove();
    
  }, [data]);

  const viz = (
    <div className="card">
      <Title title="Average Similarity of the Daily Top 200" description={DESCRIPTION} />
      <div>The closer the countries, the more similar they are.</div>
      <div className="tooltip" ref={tooltipRef}></div>
      <svg ref={svgRef} className="graph"></svg>
    </div>
  );

  return data.length > 2 ? viz : null;
}
