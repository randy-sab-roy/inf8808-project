import { useEffect, useRef } from "react";
import * as d3 from "d3";
import countries from "../../data/countries.json";
import { COLOR_CONTINENTS } from "../../constants";
import { Title } from "../title";

const DESCRIPTION = `
    Works for any number of countries selected. \n
    This visualization shows how many songs and artists appear in the daily top 200. 
    This chart is separated in 4 sections to give a general idea of the musical diversity of a country.
    High values (top part and right part) represent a high diversity.
    Low values (lower part and left part) represent a low diversity.
    It is possible to infer the rates of songs per artist from this data.
  `;

const WIDTH = 800;
const HEIGHT = 400;
const RADIUS = 10;
const PADDING = 70;
const OPACITY = 0.6;

export function MusicalVariety(props) {
  const selection = props.selection;
  const svgRef = useRef();
  const tooltipRef = useRef();

  const data = Object.entries(countries)
    .filter(c => selection.includes(c[0]))
    .map(c => ({ Country: c[0], ...c[1] }));

  useEffect(() => {
    if (data.length < 1) return;

    let g = d3.select(svgRef.current).select("g");
    const tooltip = d3.select(tooltipRef.current);

    const countriesContent = Object.values(countries);
    const songs = countriesContent.map(c => c.SongCount);
    const artists = countriesContent.map(c => c.ArtistCount);

    const xScale = d3.scaleLinear([d3.min(songs) - 349, d3.max(songs) + 599], [PADDING, WIDTH - PADDING]);
    const yScale = d3.scaleLinear([d3.min(artists) - 136, d3.max(artists) + 180], [HEIGHT - PADDING, PADDING]);

    if (g.node() == null) {
      g = d3.select(svgRef.current).attr("viewBox", `0 0 ${WIDTH} ${HEIGHT}`).append("g");
      drawLines(g, WIDTH, HEIGHT, PADDING);
      drawAxisTitles(g, WIDTH, HEIGHT);
      drawSideTitles(g, WIDTH, HEIGHT, PADDING);

      g.selectAll("text").attr("dy", ".35em").style("text-anchor", "middle");
      applyYAxis(g, yScale, PADDING);
      applyXAxis(g, xScale, HEIGHT, PADDING);
    }

    g.selectAll("circle")
      .data(data)
      .join("circle")
      .attr("r", RADIUS)
      .attr("fill", c => COLOR_CONTINENTS(c.Region))
      .attr("fill-opacity", OPACITY)
      .attr("stroke", c => COLOR_CONTINENTS(c.Region))
      .attr("stroke-width", 0.7)
      .attr("cx", c => xScale(c.SongCount))
      .attr("cy", c => yScale(c.ArtistCount))
      .on("mouseover", (d, i, a) => {
        const pos = a[i].getBoundingClientRect();
        tooltip
          .style("visibility", "visible")
          .style("left", `${pos.left + pos.width / 2}px`)
          .style("top", `${pos.top}px`)
          .html(
            `<b>${d.Name}</b></br><span>Artist Count: ${d.ArtistCount.toLocaleString(
              "en-US"
            )}</span></br><span>Song Count: ${d.SongCount.toLocaleString("en-US")}</span>`
          );
        d3.select(a[i]).attr("fill-opacity", 1).raise();
      })
      .on("mouseleave", (_, i, a) => {
        tooltip.style("visibility", "hidden");
        d3.select(a[i]).attr("fill-opacity", OPACITY);
      });
  }, [data]);

  const viz = (
    <div className="card">
      <Title title="Musical Diversity of the Selected Countries" description={DESCRIPTION} />
      <div className="tooltip" ref={tooltipRef}></div>
      <svg ref={svgRef} className="graph"></svg>
    </div>
  );

  return data.length > 0 ? viz : null;
}

// Lines
function drawLines(g, WIDTH, HEIGHT, PADDING) {
  g.append("line")
    .attr("x1", PADDING)
    .attr("y1", HEIGHT / 2)
    .attr("x2", WIDTH - PADDING)
    .attr("y2", HEIGHT / 2);
  g.append("line")
    .attr("x1", WIDTH / 2)
    .attr("y1", PADDING)
    .attr("x2", WIDTH / 2)
    .attr("y2", HEIGHT - PADDING);
  g.selectAll("line").style("stroke", "#333333").style("stroke-width", 3);
}

function drawAxisTitles(g, WIDTH, HEIGHT) {
  g.append("text")
    .attr("transform", `translate(${WIDTH / 2}, ${HEIGHT - 15})`)
    .text("Songs")
    .attr("fill", "white");
  g.append("text")
    .attr("transform", `translate(${15}, ${HEIGHT / 2}) rotate(-90)`)
    .text("Artists")
    .attr("fill", "white");
}

function drawSideTitles(g, WIDTH, HEIGHT, PADDING) {
  g.append("text")
    .attr("transform", `translate(${WIDTH - PADDING + 15}, ${PADDING + (HEIGHT - 2 * PADDING) / 4}) rotate(90)`)
    .attr("fill", "grey")
    .text("Many Artists");
  g.append("text")
    .attr("transform", `translate(${WIDTH - PADDING + 15}, ${PADDING + (3 * (HEIGHT - 2 * PADDING)) / 4}) rotate(90)`)
    .attr("fill", "grey")
    .text("Few Artists");
  g.append("text")
    .attr("transform", `translate(${PADDING + (WIDTH - 2 * PADDING) / 4}, ${PADDING - 15})`)
    .attr("fill", "grey")
    .text("Few Songs");
  g.append("text")
    .attr("transform", `translate(${PADDING + (3 * (WIDTH - 2 * PADDING)) / 4}, ${PADDING - 15})`)
    .attr("fill", "grey")
    .text("Many Songs");
}

// Axis
function applyXAxis(g, xScale, HEIGHT, PADDING) {
  let xAxis = g
    .append("g")
    .attr("transform", `translate(${0}, ${HEIGHT - PADDING})`)
    .call(d3.axisBottom(xScale))
    .selectAll("text")
    .style("text-anchor", "end")
    .attr("transform", "translate(-10,0)rotate(-45)");

  return xAxis;
}

function applyYAxis(g, yScale, PADDING) {
  let yAxis = g.append("g").attr("transform", `translate(${PADDING}, ${0})`).call(d3.axisLeft(yScale));

  return yAxis;
}
