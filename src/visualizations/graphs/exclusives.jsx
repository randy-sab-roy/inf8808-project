import { useEffect, useRef } from "react";
import * as d3 from "d3";
import countries from "../../data/countries.json";
import { COLOR_CONTINENTS, COLOR_GREY } from "../../constants";
import { Title } from "../title";

const DESCRIPTION = `
    Works for 1, 2 or 3 countries selected. \n
    This visualization shows the proportion of the artists of the top 200 charts that are exclusive to a specific country.
    A high percentage (e.g. Japan) means that many artists are not popular in any other country. 
    Conversly, a low percentage (i.e. Dom. Republic) means that the popular artists are also popular in at least one other country.
  `;

const WIDTH = 800;
const HEIGHT = 200;
const RADIUS = Math.min(WIDTH, HEIGHT) / 2.1;
const INNER_RADIUS = RADIUS / 1.6;
const TRANSITION = d3.transition().duration(100).ease(d3.easeLinear);

export function Exclusives(props) {
  const selection = props.selection;
  const svgRef = useRef();
  const tooltipRef = useRef();

  const show = selection.length > 0 && selection.length < 4;

  useEffect(() => {
    if (!show) return;

    let g = d3.select(svgRef.current).select("g");
    if (g.node() == null) {
      g = d3
        .select(svgRef.current)
        .attr("viewBox", `0 0 ${WIDTH} ${HEIGHT}`)
        .append("g")
        .attr("transform", `translate(${WIDTH / 2}, ${HEIGHT / 2})`);
    }

    const tooltip = d3.select(tooltipRef.current);

    const data = selection
      .map(code => ({
        code,
        name: countries[code].Name,
        exclusive: countries[code]["Exclusives"],
        region: countries[code].Region,
        artists: countries[code]["ExclusiveArtists"],
      }))
      .map(o => ({ ...o, exclusive: [o.exclusive, 1 - o.exclusive] }));

    const pie = d3.pie().sort(null);
    const scaleX = d3
      .scaleBand()
      .domain(data.map(d => d.code))
      .range([-WIDTH / 2, WIDTH / 2]);

    let graphs = g.selectAll("g").data(data, d => d.code);
    let added = graphs
      .enter()
      .append("g")
      .attr("transform", d => `translate(${scaleX(d.code) + scaleX.step() / 2}, 0)`);

    g.selectAll("*").interrupt();
    graphs.transition(TRANSITION).attr("transform", d => `translate(${scaleX(d.code) + scaleX.step() / 2}, 0)`);
    graphs.exit().remove();
    graphs = graphs.merge(added);

    graphs
      .selectAll("path")
      .data(
        d =>
          pie(d.exclusive).map((p, k) => ({
            ...p,
            color: k === 1 ? COLOR_GREY : COLOR_CONTINENTS(d.region),
            hoverable: k !== 1,
            data: d,
          })),
        d => d.hoverable
      )
      .join("path")
      .attr("d", d3.arc().innerRadius(INNER_RADIUS).outerRadius(RADIUS))
      .attr("fill", d => d.color);

    graphs
      .selectAll("path")
      .filter(d => d.hoverable)
      .on("mouseover", (d, i, a) => {
        d3.select(a[i]).attr("stroke-width", 2).attr("stroke", "white").raise();
        const formatedText = d.data.artists.map(artist => `<li>${artist}</li>`).join("");

        tooltip
          .style("visibility", "visible")
          .style("left", `${d3.event.pageX}px`)
          .style("top", `${d3.event.pageY + 5}px`)
          .html(`<b>Top Exclusive Artists</b></br><ol style="text-align: left;">${formatedText}</ol>`);
      })
      .on("mousemove", () => {
        tooltip.style("left", `${d3.event.pageX}px`).style("top", `${d3.event.pageY + 5}px`);
      })
      .on("mouseleave", (_, i, a) => {
        tooltip.style("visibility", "hidden");
        d3.select(a[i]).attr("stroke-width", 0);
      });

    g.selectAll(".countryNameText").remove();
    graphs
      .selectAll(".countryNameText")
      .data(d => [d])
      .join("text")
      .attr("class", ".countryNameText")
      .style("text-anchor", "middle")
      .attr("fill", "white")
      .attr("dy", "-0.2em")
      .text(d => `${d.name} `);

    g.selectAll(".countryValText").remove();
    graphs
      .selectAll(".countryValText")
      .data(d => [d])
      .join("text")
      .attr("class", ".countryValText")
      .style("text-anchor", "middle")
      .attr("fill", "white")
      .attr("dy", "1.2em")
      .text(d => `${(d.exclusive[0] * 100).toFixed(1)}%`);
  }, [selection, show]);

  const card = (
    <div className="card">
      <Title title="Percentage of Artists that are Exclusive to the Country" description={DESCRIPTION} />
      <svg ref={svgRef} className="graph" style={{ margin: "20px 0" }}></svg>
      <div className="tooltip" ref={tooltipRef}></div>
    </div>
  );

  const output = show ? card : null;

  return output;
}
