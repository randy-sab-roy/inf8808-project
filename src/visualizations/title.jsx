import "./title.css";

export function Title(props) {
  const { title, description } = props;
  return (
    <div className="title-container">
      <div className="title">{title}</div>
      <div className="handle">
        ?<div className="description-tooltip">{description}</div>
      </div>
    </div>
  );
}
