import { Exclusives } from "./graphs/exclusives";
import { MusicalVariety } from "./graphs/musicalVariety";
import { Clustering } from "./graphs/clustering";
import { TopArtists } from "./graphs/topArtists";
import countries from "../data/countries.json";
import "./visualizations.css";
import { SharedArtists } from "./graphs/sharedArtists";
import { Playlist } from "./graphs/playlist";

export function Visualizations(props) {
  const selection = props.selected;

  const header = (
    <header className="header">
      <div>
        <p className="header-title">Spotify Data Visualization</p>
        <p className="description">Select a country to begin. Different visualizations will appear in this panel depending on the number of countries selected.</p>
      </div>
    </header>
  );

  const cards = (
    <div>
      <div style={{ textAlign: "center", marginTop: "20px" }}>
        <div className="header-title">
          {selection.length > 4 ? (
            <span>{selection.length} countries selected</span>
          ) : (
            selection.map((s, i) => (
              <span key={s}>
                {" "}
                {countries[s].Name}
                {i === selection.length - 2 ? " and " : ""}
                {i < selection.length - 2 && selection.length > 1 ? ", " : ""}
              </span>
            ))
          )}
        </div>
      </div>
      <div className="card-container">
        <Exclusives selection={selection} />
        <SharedArtists selection={selection} />
        <TopArtists selection={selection} />
        <MusicalVariety selection={selection} />
        <Clustering selection={selection} />
        <Playlist selection={selection} />
      </div>
    </div>
  );

  const display = selection.length > 0 ? cards : header;

  return <div>{display}</div>;
}
