import "./presentation.css";

export function Presentation({ onButtonClick }) {
  return (
    <div className="background">
      <div>
        <div className="presentation-title">Spotify Visualization</div>
        <div className="presentation-description">
          <p>This project highlights the musical characteristics of different countries around the world.</p>
          <p>
            By comparing the countries, we can see how diverse the music is, and how similar cultures have similar
            habits regarding online music streaming.
          </p>
        </div>
        <button className="button" onClick={onButtonClick}>
          Explore the visualizations
        </button>
      </div>
    </div>
  );
}
