import * as d3 from "d3";
import countries from "./data/countries.json";

export const COLOR_SCHEME = ["#1DB954", "#FC7E00", "#009FFF", "#BA27A9", "#BA110B", "#BA9A14", "#27BAAE"];
export const COLOR_GREY = "#4D4D4D";

export const COLOR_CONTINENTS = d3
  .scaleOrdinal()
  .domain(new Set(Object.values(countries).map(c => c.Region)))
  .range(COLOR_SCHEME);
