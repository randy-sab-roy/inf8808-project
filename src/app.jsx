import { useState } from "react";
import "./app.css";
import { Visualizations } from "./visualizations/visualizations";
import { Selection } from "./selection/selection";
import { Presentation } from "./presentation/presentation";

function toggleState(stateSetter, value) {
  stateSetter(p => {
    if (p.includes(value)) return p.filter(v => v !== value);
    else return [...p, value];
  });
}

export default function App() {
  const [selected, setSelected] = useState([]);
  const [hovered, setHovered] = useState([]);
  const [passedPresentation, setPassedPresentation] = useState(false);

  const presentation = <Presentation onButtonClick={() => setPassedPresentation(true)} />;
  const content = (
    <div>
      <div id="selection-panel">
        <Selection
          selected={selected}
          hovered={hovered}
          onSelectNone={() => setSelected([])}
          onSelectAll={a => setSelected(a)}
          onSelect={code => toggleState(setSelected, code)}
          onHover={code => toggleState(setHovered, code)}
        ></Selection>
      </div>
      <div id="visualization-panel">
        <Visualizations selected={selected}></Visualizations>
      </div>
    </div>
  );

  return passedPresentation ? content : presentation;
}
