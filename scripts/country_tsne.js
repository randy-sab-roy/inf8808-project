const tsnejs = require("./tsne.js");
const fs = require("fs");

const data = JSON.parse(fs.readFileSync("similarity.json"));
const countries = JSON.parse(fs.readFileSync("countries.json"));

const opt = {
  epsilon: 10,
  perplexity: 20,
  dim: 2,
};

const tsne = new tsnejs.tSNE(opt);

for (const key in data) {
  data[key][key] = 1.0;
}

const dists = Object.values(data).map(e =>
  Object.entries(e)
    .sort((a, b) => (a[0] < b[0] ? -1 : 1))
    .map(a => 1 - Number.parseFloat(a[1]))
);

tsne.initDataDist(dists);

for (var k = 0; k < 500; k++) {
  tsne.step();
}

const keys = Object.keys(data);
const solution = tsne.getSolution();
solution.forEach((s, i) => {
  countries[keys[i]].x = s[0];
  countries[keys[i]].y = s[1];
});

fs.writeFileSync("../src/data/countries.json", JSON.stringify(countries));
