## This script computes the total of streams by artist weekly, using only #1 song of the day. It then computes the streaks. 
# %% imports
import os
import pandas as pd
from os import listdir
from os.path import isfile
import numpy as np
import json

path = os.path.join("..\data", "Countries")
onlyfiles = [f for f in listdir(path) if isfile(os.path.join(path, f))]
# %%
print(onlyfiles)
# %%
def getRegionFromFileName(file_name):
    return file_name.split('.')[0].split('_')[-1]

# %%
for country_i in range(len(onlyfiles)):
    df = pd.read_csv(os.path.join(path, onlyfiles[country_i])).dropna()
    df = df.loc[df['Position'] == 1.0]
    weeks = {}
    week_artists = {}
    week_count = 0
    for index, row in df.iterrows():
        if row['Artist'] != "NA" and row['Artist'] != "Artist":
            if row['Artist'] not in week_artists:
                week_artists[row['Artist']] = row['Streams']
            else:
                week_artists[row['Artist']] += row['Streams']
            if index % 7 == 0 and index != 0:
                # new week
                top_artist = list(np.flip([ k for k, v in sorted(week_artists.items(), key=lambda item: item[1])]))[0]
                weeks[week_count] = {"Artist": top_artist, 'Streams': week_artists[top_artist]}
                week_count += 1
                week_artists = {}

    current_top = ""
    begin = 0
    end = 0
    json_file = {}
    for i in range(len(weeks)):
        a = weeks[i]['Artist']
        if current_top == "":
            current_top = a
            begin = i
        else:
            if current_top != a:
                end = i
                if current_top in json_file:
                    json_file[current_top].append({'begin': begin, 'end': i-1})
                else:
                    json_file[current_top] = [{'begin': begin, 'end': i-1}]
                begin = end
                current_top = a
    
                
    with open(os.path.join("..\data\\Streaks", f"{getRegionFromFileName(onlyfiles[country_i])}.json"), "w", encoding='utf8') as outfile:  
        json.dump(json_file, outfile, ensure_ascii=False) 


# %%

# %%
