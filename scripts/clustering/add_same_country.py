#%%
from sklearn_extra.cluster import KMedoids
from sklearn.cluster import AgglomerativeClustering
import numpy as np
import json
from sklearn.datasets import make_blobs
from sklearn.preprocessing import StandardScaler
from pyclustering.cluster.kmedoids import kmedoids
from pyclustering.cluster import cluster_visualizer
from pyclustering.utils import read_sample
from pyclustering.samples.definitions import FCPS_SAMPLES
import matplotlib.pyplot as plt
from sklearn.ensemble import RandomForestClassifier
from sklearn.base import BaseEstimator, clone
from sklearn.utils.metaestimators import if_delegate_has_method
import pandas as pd
from sklearn.manifold import MDS
# %%
with open(os.path.join(os.getcwd(),'similarity.json'), encoding='utf8') as json_file:
    data = json.load(json_file)

for k in data.keys():
    print(k)
    data[k][k] = 1
    sortedDict = dict( sorted(data[k].items(), key=lambda x: x[0].lower()) )
    data[k] = sortedDict
# %%
with open("similarity2.json", "w", encoding='utf8') as outfile:  
    json.dump(data, outfile, ensure_ascii=False) 
# %%
