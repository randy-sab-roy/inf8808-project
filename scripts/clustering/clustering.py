#%%
from sklearn_extra.cluster import KMedoids
from sklearn.cluster import AgglomerativeClustering
import numpy as np
import json
from sklearn.datasets import make_blobs
from sklearn.preprocessing import StandardScaler
from pyclustering.cluster.kmedoids import kmedoids
from pyclustering.cluster import cluster_visualizer
from pyclustering.utils import read_sample
from pyclustering.samples.definitions import FCPS_SAMPLES
import matplotlib.pyplot as plt
from sklearn.ensemble import RandomForestClassifier
from sklearn.base import BaseEstimator, clone
from sklearn.utils.metaestimators import if_delegate_has_method
import pandas as pd
from sklearn.manifold import MDS
# %%
with open(os.path.join(os.getcwd(),'similarity2.json'), encoding='utf8') as json_file:
    data = json.load(json_file)

# %%
data
# %%
matrix = np.zeros((len(data), len(data)))
# %%
l_data = list(data)
# %%
d = {}
for i, x in enumerate(l_data):
    d[x] =i
# %%
l_data
# %%
for e in l_data:
    for name in data[e].keys():
        matrix[d[e], d[name]] = 1/data[e][name]
# %%
X = matrix
#%%
def plot_scatter(X,  color, alpha=0.7):
    return plt.scatter(X[:, 0],
                       X[:, 1],
                       c=color,
                       alpha=alpha,
                       edgecolor='k')

#%%
cluster = AgglomerativeClustering(affinity='precomputed', linkage='complete',compute_full_tree=True, n_clusters=5)
cluster_labels = cluster.fit_predict(X)


plt.figure(figsize=(30, 10))

plt.subplot(131)
plot_scatter(X, cluster_labels)
plt.title("Clustering")

plt.show()
#%%
# %%
cluster_data = {}
for i , x in enumerate(cluster_labels):
    cluster_data[l_data[i]]  = x
# %%
clustr_ids = set(cluster_data.values())
cluster_array = {}
for cid in clustr_ids:
    countries = [k for k, v in cluster_data.items() if v == cid]
    cluster_array[str(cid)] = countries

# %%
with open("clusters.json", "w", encoding='utf8') as outfile:  
    json.dump(cluster_array, outfile, ensure_ascii=False) 
 # %%
embedding = MDS(n_components=2,n_init=4, max_iter=300, dissimilarity='precomputed')
X_transformed = embedding.fit_transform(X)
X_transformed.shape
plot_scatter(X_transformed, cluster_labels)
# %%
coords = pd.DataFrame(columns=['code', 'x', 'y'])
coords['code'] = l_data
# %%
coords = coords.set_index('code')
# %%
coords['x'] = X_transformed[:,0]
# %%
coords['y'] = X_transformed[:,1]
# %%
coords.to_csv(path_or_buf='coords.csv')

# %%
