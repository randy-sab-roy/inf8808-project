# %%
import os
import pandas as pd
from os import listdir
from os.path import isfile, join
import math
import sys
import matplotlib.pyplot as plt
import seaborn as sns
import json

# %%
path = "..\data"
countries = os.path.join(path,"Countries")
onlyfiles = [f for f in listdir(countries) if isfile(join(countries, f))]


# %%
x = []
y = []
colors = {'North America': 'red','Europe':'green', 'Asia':'blue', 'Oceania':'yellow', 'South America':'orange'}
LUT = pd.read_csv(os.path.join(path, 'Global', 'continents.csv'))
LUT = LUT.set_index(['Two_Letter_Country_Code'])
# %%
continent = []
keys = []
for country_i in range(len(onlyfiles)):
    df = pd.read_csv(os.path.join(countries, onlyfiles[country_i])).dropna()
    norm = len(df)
    x.append(df['Artist'].nunique())
    y.append(df['spotify_id'].nunique())
    key = str(df['region'].values[0]).upper()
    print(key)
    n = LUT.loc[key]['Continent_Name']
    print(n)
    keys.append(key)
    continent.append(n)

#
FINAL = pd.DataFrame(dict(artistes=x, chansons=y, continent = continent, keys= keys))
#%%
sns.lmplot( data=FINAL, x='artistes', y='chansons', hue='continent', fit_reg=False)
plt.ylim(0, 8000)
plt.xlim(0, 1900)
plt.show()
# %%
with open(os.path.join(path,'countries.json'), encoding='utf8') as json_file:
    data = json.load(json_file)
# %%
for i, row in FINAL.iterrows():
    c_name = row['keys'].lower()
    data[c_name]['ArtistCount'] = row['artistes']
    data[c_name]['SongCount'] = row['chansons']
# %%
with open((os.path.join(os.getcwd(),'countries.json')), "w", encoding='utf8') as outfile:  
    json.dump(data, outfile, ensure_ascii=False) 
# %%
