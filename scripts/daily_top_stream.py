# %%
import os
import pandas as pd
from os import listdir
from os.path import isfile, join
import math
import sys
import matplotlib.pyplot as plt
import seaborn as sns
import json

# %%
path = "..\data\\Countries"
onlyfiles = [f for f in listdir(path) if isfile(join(path, f))]
ALL_STREAMS = {}

# %%
for country_i in range(len(onlyfiles)):
    json_data = []
    df = pd.read_csv(os.path.join(path, onlyfiles[country_i]))
    print(onlyfiles[country_i])
    first = df.loc[df['Position'] == 1.0]
    values = first['Streams'].values
    avg = 7.0 * sum(values)/len(values)
    temp_total = 0
    count = 0
    date = first.iloc[0]['date']
    for i, row in first.iterrows():
        if count % 7 == 0 and count != 0:
            json_data.append({'Date' : date, 'Streams' :temp_total/avg, 'Country' : onlyfiles[country_i].split('.')[0] })
            date = row['date']
            temp_total = row['Streams']
        else:
            temp_total += row['Streams']
        count += 1

    ALL_STREAMS[onlyfiles[country_i].split('.')[0]] = json_data
with open(os.path.join("..\data\\Weekly_top", "all_streams.json"), "w", encoding='utf8') as outfile:  
    json.dump(ALL_STREAMS, outfile, ensure_ascii=False) 
