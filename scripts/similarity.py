# %%
import os
import pandas as pd
import time
import numpy as np
from joblib import Parallel, delayed
from operator import add, mul
from os.path import isfile, join
from os import listdir
import json
#%%
def getScoreFromDate(date, dfA, dfB):
    dateA = dfA.loc[date].set_index('spotify_id')
    dateB = dfB.loc[date].set_index('spotify_id')
    if len(dateA) == len(dateB):
        ids_a = set(dateA.index.values)
        ids_b = set(dateB.index.values)
        ids_c = ids_a.intersection(ids_b)

        tempA = dateA.index.isin(ids_c)
        common_a = dateA[tempA].copy()
        u_a = dateA[~tempA].copy()

        tempB = dateB.index.isin(ids_c)
        common_b = dateB[tempB].copy()
        u_b = dateB[~tempB].copy()

        common_a['date'] = common_b['Streams']
        common_a['region'] = common_b['Position']
        common_a = common_a.assign(Differential = lambda x: 1/(abs(x['Position'] - x['region'])+1), Total =lambda x: x['Streams'] + x['date'])
        
        date_scores = common_a['Differential'].values
        date_avgs = common_a['Total'].values
        listA = sorted(u_a['Streams'].values)
        listB = sorted(u_b['Streams'].values)

        if len(u_a)>0:
            date_scores = np.append(date_scores, np.zeros(len(u_a)))
            date_avgs = np.append(date_avgs, list( map(add, listA, listB)))

        # compute weighted score
        score = sum(list(map(mul, date_avgs, date_scores)))/(sum(date_avgs))

        ############################
        # total_score = 0
        # print(date_avgs)
        # for i in range(len(date_scores)):
        #     total_score += date_avgs[i]*date_scores[i]
        # score = total_score/(sum(date_avgs))


        # cumul += (time.time() - start)
        # print(cumul)
        return score
    # else:
    #     print(date)
# %%
path =  os.path.join('..\\data','countries')
onlyfiles = [f for f in listdir(path) if isfile(join(path, f))]
# %%
def getRegionFromFileName(file_name):
    return file_name.split('\\')[-1].split('.')[0]
# %%
# c1 = "ca"
# c2 = 'ca'
# c1 = os.path.join('..\\data','countries', f'{c1}.csv')
# c2 = os.path.join('..\\data', 'countries', f'{c2}.csv')
global_dict = {}
for i in range(len(onlyfiles)):
    local_dict = {}
    c1 = onlyfiles[i]
    c1 = os.path.join('..\\data','Countries', c1)
    dfA = pd.read_csv(c1).dropna()
    for j in range(i+1, len(onlyfiles)):
        c2 = onlyfiles[j]
        c2 = os.path.join('..\\data', 'Countries', c2)
        dfB = pd.read_csv(c2).dropna()
        setA = set(dfA['date'].values)
        setB = set(dfB['date'].values)
        setC = setA.intersection(setB)
        date_A = dfA.loc[dfA['date'].isin(setC)].set_index('date')
        date_B = dfB.loc[dfB['date'].isin(setC)].set_index('date')
        start = time.time()
        results = Parallel(n_jobs=1, prefer="threads")(delayed(getScoreFromDate)(date, date_A, date_B) for date in setC)
        results = list(filter(None, results))
        local_dict[getRegionFromFileName(c2)] = sum(results)/len(results)
    
    local_name = getRegionFromFileName(c1)
    if local_name in global_dict.keys():
        global_dict[local_name].update(local_dict)
    else:
        global_dict[local_name] = local_dict
    for k in local_dict.keys():
        if k in global_dict.keys():
            global_dict[k].update({local_name :local_dict[k]})
        else:
            global_dict[k] = {local_name :local_dict[k]}
    print(f'{i}: {c1}')
# %%
with open("similarity.json", "w", encoding='utf8') as outfile:  
    json.dump(global_dict, outfile, ensure_ascii=False) 

# %%
