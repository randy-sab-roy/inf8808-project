#%%
import os
import pandas as pd
from os import listdir
from os.path import isfile, join
import json
# %%
with open('..\\data\\Top_of_the_week\\all_weeks.json', encoding='utf8') as f:
  data = json.load(f)
#%%
path = "..\data\\Countries"
onlyfiles = [f for f in listdir(path) if isfile(join(path, f))]
new_data = {}
# %%
for country_i in range(len(onlyfiles)):
    country_name = onlyfiles[country_i].split('.')[0] 
    temp_dict = {}
    array = data[country_name]
    for elem in array:
        if elem['artist'] in temp_dict:
            temp_dict[elem['artist']] += 1
        else:
            temp_dict[elem['artist']] = 1
    new_data[country_name] = temp_dict
#%%
formatted_data = {}
for country_i in range(len(onlyfiles)):
    country_name = onlyfiles[country_i].split('.')[0] 
    temp_array = []
    for artist in new_data[country_name].keys():
        temp_array.append({'Artist': artist, 'Stream': new_data[country_name][artist]})
    formatted_data[country_name] = temp_array
# %%
with open((os.path.join(os.getcwd(),'top_of_the_week.json')), "w", encoding='utf8') as outfile:  
    json.dump(formatted_data, outfile, ensure_ascii=False) 
# %%
