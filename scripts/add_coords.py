# %%
import os
import pandas as pd
from os import listdir
from os.path import isfile, join
import math
import sys
import matplotlib.pyplot as plt
import json

# %%
path = "..\data"
df = pd.read_csv(filepath_or_buffer=os.path.join(os.getcwd(),'coords.csv'))
with open(os.path.join(path,'countries.json'), encoding='utf8') as json_file:
    data = json.load(json_file)
# %%
for i, row in df.iterrows():
    code = row['code']
    data[code]['x'] = row['x']
    data[code]['y'] = row['y']
# %%
with open((os.path.join(os.getcwd(),'countries.json')), "w", encoding='utf8') as outfile:  
    json.dump(data, outfile, ensure_ascii=False) 
