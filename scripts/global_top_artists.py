# %%
import os
import pandas as pd
from os import listdir
from os.path import isfile, join
import math
import sys

# %%
path = "..\data\\top\\"
onlyfiles = [f for f in listdir(path) if isfile(join(path, f))]


# %%
totals = []
artists = {}
for country_i in range(len(onlyfiles)):
    df = pd.read_csv(os.path.join(path, onlyfiles[country_i])).dropna()
    total = 0
    for index, row in df.iterrows():
        streams = row['Streams']
        total += streams
        artist = row['Artist'] 
        if artist in artists:
            artists[artist]['Count'] += 1
            artists[artist]['Total'] += streams
        else:
            artists[artist] = {'Total' : streams, 'Count': 1, "Name": artist}
    totals.append(total)
# %%
print(sum(totals))

#%%
df2 = pd.DataFrame(columns=['Name', 'Total', 'Count'])
for a in list(artists.keys()):
    df2 = df2.append({"Name": artists[a]['Name'], 'Total': artists[a]['Total'], "Count" : artists[a]['Count']}, ignore_index=True)

df2 = df2.sort_values(by=['Count'], ascending=False, ignore_index=True)
df2.to_csv(index=False, path_or_buf=os.path.join("..\data", "global.csv"))
# %%
