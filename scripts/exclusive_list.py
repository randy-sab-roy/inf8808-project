# %%
import os
import pandas as pd
from os import listdir
from os.path import isfile, join
import math
import sys
import numpy as np
import json

# %%
path = "..\\data\\Top\\"
onlyfiles = [f for f in listdir(path) if isfile(join(path, f))]

# %%
def getRegionFromFileName(file_name):
    return file_name.split('.')[0].split('_')[-1]

# %%
artists = {}
for country_i in range(len(onlyfiles)):
    print(country_i)
    df = pd.read_csv(os.path.join(path, onlyfiles[country_i])).dropna()
    total = 0
    country_name = getRegionFromFileName(onlyfiles[country_i])
    for index, row in df.iterrows():
        streams = row['Streams']
        artist = row['Artist'] 
        if artist in artists:
            artists[artist]['Count'] += 1
            artists[artist]['Total'] += streams
            artists[artist]['Countries'].append(country_name)
        else:
            artists[artist] = {'Total' : streams, 'Count': 1, 'Countries': [country_name], "Name": artist}

# %%
country_exclusives = {}
for a in artists:
    a = artists[a]
    if a['Count'] == 1:
        country_code = a['Countries'][0]
        if country_code in country_exclusives:
            country_exclusives[country_code][a['Name']] = a['Total']
        else:
            country_exclusives[country_code] = {a['Name']:a['Total']}
# %%
country_exclusives['ca']
# %%
top_exclusives = {}
for c in country_exclusives:
    obj = country_exclusives[c]
    top_exclusives[c] = list(np.flip([ k for k, v in sorted(obj.items(), key=lambda item: item[1])])[:5])
    
# %%
top_exclusives['fr']
# %%
for e in top_exclusives['ca']:
    print(e)
# %%
with open("top_exclusives.json", "w", encoding='utf8') as outfile:  
    json.dump(top_exclusives, outfile, ensure_ascii=False) 
# %%
with open("artist_countries.json", "w", encoding='utf8') as outfile:  
    json.dump(artists, outfile, ensure_ascii=False) 
# %%
country_counts = {}
for country_i in range(len(onlyfiles)):
    df = pd.read_csv(os.path.join(path, onlyfiles[country_i])).dropna()
    country_counts[getRegionFromFileName(onlyfiles[country_i])] = len(df)
# %%
country_counts['ca']
# %%
with open("country_counts.json", "w", encoding='utf8') as outfile:  
    json.dump(country_counts, outfile, ensure_ascii=False) 

# %%
FINAL_JSON = {}
for country_i in range(len(onlyfiles)):
    c_name = getRegionFromFileName(onlyfiles[country_i])
    exclusive_count = len(country_exclusives[c_name])
    artist_count = country_counts[c_name]
    exclusive_artists = top_exclusives[c_name]
    FINAL_JSON[c_name] = {'Exclusives': exclusive_count/artist_count, 'ExclusiveArtists' : exclusive_artists}

# %%
with open("final_exclusive.json", "w", encoding='utf8') as outfile:  
    json.dump(FINAL_JSON, outfile, ensure_ascii=False) 
# %%
