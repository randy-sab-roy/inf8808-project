# %%
import os
import pandas as pd
from os import listdir
from os.path import isfile, join
import math
import sys
import json

# %%
path = "..\data\\Countries"
onlyfiles = [f for f in listdir(path) if isfile(join(path, f))]
ALL_STREAMS = {}
def keywithmaxval(d):
     """ a) create a list of the dict's keys and values; 
         b) return the key with the max value"""  
     v=list(d.values())
     k=list(d.keys())
     return k[v.index(max(v))]

# %%
for country_i in range(len(onlyfiles)):
    country_name = onlyfiles[country_i].split('.')[0] 
    json_data = []
    df = pd.read_csv(os.path.join(path, onlyfiles[country_i])).dropna()
    print(onlyfiles[country_i])
    date = df.iloc[0]['date']
    songs = {}
    count = 0
    for i, row in df.iterrows():
        if count % 1400 == 0 and count != 0:
            top_id = keywithmaxval(songs)
            temp_row = df.loc[df['spotify_id'] == top_id].iloc[0]
            json_data.append({'date':date, 'total': songs[top_id],'song': temp_row['Track Name'], 'id' : [temp_row['spotify_id']], 'artist' : temp_row['Artist']})
            songs = {}
            date = row['date']
        else:
            temp_total = row['Streams']
            song_id = row['spotify_id']
            if song_id in songs:
                songs[song_id] += temp_total
            else:
                songs[song_id] = temp_total
        count += 1

    ALL_STREAMS[onlyfiles[country_i].split('.')[0]] = json_data
with open(os.path.join("..\data\\Top_of_the_week", "all_weeks2.json"), "w", encoding='utf8') as outfile:  
    json.dump(ALL_STREAMS, outfile, ensure_ascii=False) 




# %%
