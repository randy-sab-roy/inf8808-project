# %%
import os
from os import listdir
from os.path import isfile, join
import sys
import json

# %%
path = "..\data"
countries = os.path.join(path,"Countries")
# %%
with open(os.path.join(os.getcwd(),path, 'artist_countries.json'), encoding='utf8') as json_file:
    data = json.load(json_file)
# %%
artist_lists = {}
for artist in data:
    name = data[artist]['Name']
    for country_code in data[artist]['Countries']:
        if country_code in artist_lists:
            artist_lists[country_code].append(name)
        else:
            artist_lists[country_code] = [name]
# %%
with open("country_artists.json", "w", encoding='utf8') as outfile:  
    json.dump(artist_lists, outfile, ensure_ascii=False) 
