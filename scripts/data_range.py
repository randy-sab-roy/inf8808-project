# %%
import os
import pandas as pd
from os import listdir
from os.path import isfile, join
import math
import sys

# %%
path = os.path.join(os.pardir , 'src','data')
onlyfiles = [f for f in listdir(path) if isfile(join(path, f))]


# %%
smallest = sys.maxsize * 2 + 1
biggest = 0
averages = []
for country_i in range(len(onlyfiles)):
    current_path = os.path.join(os.pardir,'src',"data", onlyfiles[country_i])
    print('PATH ', current_path)
    df = pd.read_csv(current_path,header=None)
    print(df)
    # .dropna()
    temp_total = 0
    for index, row in df.iterrows():
        s = row['Streams']
        if s < smallest:
            smallest = s
        elif s > biggest:
            biggest = s
        temp_total += s

    averages.append(temp_total/len(df))
print(biggest)
print(smallest)

#%%
total = 0
for i in averages:
    total += i

print(total/len(averages))
