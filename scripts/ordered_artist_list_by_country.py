# %%
import os
import pandas as pd
from os import listdir
from os.path import isfile, join
import math
import sys
import matplotlib.pyplot as plt
import json

# %%
path = "..\data\\Top2"
onlyfiles = [f for f in listdir(path) if isfile(join(path, f))]
def getCountryNameFromFile(fileName):
    return fileName.split('_')[-1].split('.')[0]
# %%
artist_list_dict = {}
for country in onlyfiles:
    artist_list_dict[getCountryNameFromFile(country)] = list(pd.read_csv(os.path.join(path, country)).sort_values(by=['Streams'], ascending=False)['Artist'].values)

# %%
with open("artist_list_by_country.json", "w", encoding='utf8') as outfile:  
    json.dump(artist_list_dict, outfile, ensure_ascii=False) 
# %%
